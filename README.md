<div>
A commit I found on Leendert's spectrum generating code...
<img src="slowness.png"/>
<\div>
<hr>

<div>
Most of the files here are files I found online. 
Honestly one of the best parts of jupyter-python is that there's so much support; if you ask me for help and I haven't seen it before I'll just google the documentation and stack overflow. I can't remember ever having a problem that one of these couldn't solve. 
Anyhow, here's a list of useful links and what they are.
<\div>
<hr>


<a href="https://web.eecs.utk.edu/~bvz/cs365/lecture_notes.html">class notes</a> 
If you scroll down to the bottom there is a section on python. This comes from a professor of mine and focuses on really knowing the ins and outs of python. If you read this, it will make your python more terse and readable. It's not essential to getting work done, but it gives you the tools that make python so great. 
<hr>


<a href="https://github.com/jupyter/jupyter/wiki/A-gallery-of-interesting-Jupyter-Notebooks">Interesting Jupyter notebooks</a>  This has every link under the sun. If you want to learn how to add numbers in python (hint: it's just x+y), or make your matplotlib plots look like XKCD comics, this has links for that<hr>


<a href="https://docs.scipy.org/doc/numpy/reference/">Numpy Reference</a>  If you want to look up how to transpose a matrix, this is the place (hint: it's myMatrix.T, pretty intuitive right?)<hr>


<a href="https://docs.scipy.org/doc/scipy/reference/">Scipy Reference</a>  I would bet that someone's done the hard work of typing out all the physical constants into a python module, if only there was some sort of way to find where that work was done...<hr>





